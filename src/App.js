import './App.scss';
import { useSelector, } from 'react-redux';
import {
  FileHandler,
  FileViewer,
  Step,
  FileTypeChooser
} from './components';

const App = () => {
  const fileData = useSelector(state => state.app.fileData);
  const wizardStep = useSelector(state => state.app.wizardStep);
  const fileType = useSelector(state => state.app.fileType);

  return (
    <div className="m-file-tester">
      <div className="header">
        <span className="label">Instructions</span>
      </div>
      <div className="m-file-tester__wizard-steps">
        <Step isActive={wizardStep === 1} step="1" description="Select a file you wish to process." />
        <Step isActive={wizardStep === 2} step="2" description="Choose to use CSV (comma-separated values) or TSV (tab-separated values) processing." />
        <Step isActive={wizardStep === 3} step="3" description="Review output" />
      </div>
      {wizardStep === 1 && <FileHandler />}
      {wizardStep === 2 && <FileTypeChooser fileType={fileType} />}
      {wizardStep === 3 && <FileViewer fileData={fileData} fileType={fileType} />}
    </div>
  );
}

export default App;
