import { actionTypes } from './types';
import produce from 'immer';

const appInitialState = {
  fileData: '',
  wizardStep: 1,
  completedStep: 0,
  fileType: ''
};

const app = produce((draft = appInitialState, action) => {
  const { type } = action;
  switch (type) {
    case actionTypes.SET_FILE_DATA:
      draft.fileData = action.state;
      break;
    case actionTypes.SET_WIZARD_STEP:
      draft.wizardStep = action.state;
      break;
    case actionTypes.SET_FILE_TYPE:
      draft.fileType = action.state;
      break;
    case actionTypes.SET_COMPLETED_STEP:
      draft.completedStep = action.state;
      break;
    default:
      return draft;
  }
});

export default app;
