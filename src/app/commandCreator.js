import { actionTypes } from './types';

export const ReduxCommandCreator = (dispatch) => {
  return {
    SetFileData(state) {
      dispatch({
        type: actionTypes.SET_FILE_DATA,
        state
      });
    },
    SetWizardStep(state) {
      dispatch({
        type: actionTypes.SET_WIZARD_STEP,
        state
      });
    },
    SetFileType(state) {
      dispatch({
        type: actionTypes.SET_FILE_TYPE,
        state
      });
    },
    SetCompletedStep(state) {
      dispatch({
        type: actionTypes.SET_COMPLETED_STEP,
        state
      });
    },
  };
};
