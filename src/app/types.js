const AppKey = 'fileTester-';
export const actionTypes = {
  SET_FILE_DATA: `${AppKey}SET_FILE_DATA`,
  SET_WIZARD_STEP: `${AppKey}SET_WIZARD_STEP`,
  SET_FILE_TYPE: `${AppKey}SET_FILE_TYPE`,
  SET_COMPLETED_STEP: `${AppKey}SET_COMPLETED_STEP`,
};

