import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { ReduxCommandCreator } from '../app/commandCreator';

const FileTypeChooser = props => {
  const {
    fileType
  } = props;

  const dispatch = useDispatch();
  const rootReduxCommands = ReduxCommandCreator(dispatch);

  const fileTypeHandler = type => {
    rootReduxCommands.SetWizardStep(3);
    rootReduxCommands.SetCompletedStep(2);
    rootReduxCommands.SetFileType(type);
  };

  const blockEl = 'm-file-tester__file-type-chooser';
  return (
    <div className={blockEl}>
      <div
        onClick={() => fileTypeHandler('csv')}
        className={fileType === 'csv' ? 'is-active' : ''}>
        CSV
      </div>
      <div
        onClick={() => fileTypeHandler('tsv')}
        className={fileType === 'tsv' ? 'is-active' : ''}>
        TSV
      </div>
    </div>
  )
};

FileTypeChooser.defaultProps = {
  fileType: 'csv',
};

FileTypeChooser.propTypes = {
  fileType: PropTypes.string,
};

export { FileTypeChooser };
export default FileTypeChooser;
