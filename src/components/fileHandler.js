import React from 'react';
import { useDispatch } from 'react-redux';
import { ReduxCommandCreator } from '../app/commandCreator';

const FileHandler = () => {
  const dispatch = useDispatch();
  const rootReduxCommands = ReduxCommandCreator(dispatch);

  let reader = new FileReader();
  const onFileChange = event => {
    reader.onload = (evt) => {
      if (evt.target.readyState !== 2) return;
      if (evt.target.error) {
        alert('Error while reading file');
        return;
      }

      rootReduxCommands.SetFileData(evt.target.result);
      rootReduxCommands.SetFileType('');
      rootReduxCommands.SetCompletedStep(1);
      rootReduxCommands.SetWizardStep(2);
    };

    reader.readAsText(event.target.files[0]);
  };

  const blockEl = 'm-file-tester__file-handler';

  return (
    <div className={blockEl}>
      <span>Select the file you would like to process by clicking the Choose File button</span>
      <div className="file-field">
        <input accept=".txt, .csv, .tsv" type="file" onChange={onFileChange} />
      </div>
    </div>
  )
};

export { FileHandler };
export default FileHandler;
