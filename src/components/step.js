import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { ReduxCommandCreator } from '../app/commandCreator';

const Step = props => {
  const {
    step,
    description,
    isActive,
  } = props;
  const dispatch = useDispatch();
  const rootReduxCommands = ReduxCommandCreator(dispatch);
  const completedStep = useSelector(state => state.app.completedStep);
  const stepHandler = () => {
    if ((completedStep + 1) >= step) {
      rootReduxCommands.SetWizardStep(parseInt(step));
    }
  };

  return (
    <div onClick={stepHandler} className={`step ${isActive ? 'is-active' : ''}`}>
      <span className="label">{step}</span>
      <span className="description">{description}</span>
    </div>
  );
};

Step.defaultProps = {
  step: 0,
  description: '',
  isActive: false,
};

Step.propTypes = {
  fileData: PropTypes.number,
  description: PropTypes.string,
  isActive: PropTypes.bool,
};

export { Step };
export default Step;
