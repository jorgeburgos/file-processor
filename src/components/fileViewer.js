import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

const FileViewer = props => {
  const {
    fileData,
    fileType
  } = props;


  const Field = ({ label }) => {
    return (<div className="field">{label}</div>)
  };

  const Row = ({ fields }) => {
    return (
      <div className="row">
        {
          fields.map((item, index) => {
            return <Field label={item} key={index} />;
          })
        }
      </div>
    );
  };

  const processFile = (fileData, fileType) => {
    const splitRule = fileType === 'csv' ? ',' : '\t'
    const recParse = fileData.split('\n')
    const headerList = recParse[0].split(splitRule);
    const headerFieldCount = headerList.length;
    const recList = recParse.slice(1, recParse.length);

    const goodData = [];
    const badData = [];
    recList.forEach((item, index) => {
      const fields = item.split(splitRule);
      if (fields.length === headerFieldCount) {
        goodData.push(fields);
      } else {
        badData.push(fields);
      }
    })

    const downloadData = (data, fileName) => {
      const element = document.createElement("a");
      const file = new Blob([data], { type: 'text/plain' });
      element.href = URL.createObjectURL(file);
      element.download = fileName;
      document.body.appendChild(element); // Required for this to work in FireFox
      element.click();
    };

    return (
      <Fragment>
        { goodData && goodData.length > 0 && (
          <div className="good-data-container">
            <span className="header">Good data</span>
            <span className="download-link" onClick={() => downloadData(goodData.join('\n'), 'good-data.txt')}>Download</span>
            <Row fields={headerList} />
            {
              goodData.map((item, index) => {
                return <Row key={index} fields={item} />;
              })
            }
          </div>
        )}

        {badData && badData.length > 0 && (
          <div className="bad-data-container">
            <span className="header">Bad data</span>
            <span className="download-link" onClick={() => downloadData(badData.join('\n'), 'bad-data.txt')}>Download</span>
            {
              badData.map((item, index) => {
                return <Row key={index} fields={item} />;;
              })
            }
          </div>
        )}

      </Fragment>
    )
  }

  const blockEl = 'm-file-tester__file-viewer';
  return (
    <div className={blockEl}>
      <span className="header">File Data Viewer</span>
      <div className="data">
        {processFile(fileData, fileType)}
      </div>
    </div>
  );
};

FileViewer.defaultProps = {
  fileData: '',
  fileType: '',
};

FileViewer.propTypes = {
  fileData: PropTypes.string,
  fileType: PropTypes.string,
};

export { FileViewer };
export default FileViewer;
